const mongoose = require("mongoose")
const UserModel = require("../models/User")
const NotFoundError = require("../exceptions/NotFoundError")


const protectedFields  = { 
    __v: false,
    password: false
};

var UserRepository = class UserRepository {

    async create(data) {
        const newUser = new UserModel({ ...data })
        await newUser.save()
        const user = this.getOneById(String(newUser._id))
        return user
    }

    async getOneByFeild(filedName, value) {
        return await UserModel.findOne({[filedName]: value}, protectedFields)
    }

    async getOneById(id) {
        return await this.getOneByFeild("_id", new mongoose.Types.ObjectId(id))
    }

    //TODO при удалении пользователя удалить все связанные уроки и пользовательские связи и сертификаты пользователей
    async deleteOneById(id) {
        const deleteUserRes = await UserModel.deleteOne({ _id: new mongoose.Types.ObjectId(id)})

        if (deleteUserRes.deletedCount === 0) {
            throw new NotFoundError(`User not found`)
        }
    }

    async getAll() {
        return await UserModel.find({}, protectedFields).exec()
    }

    async update(id, data) {

        const user = await this.getOneByFeild("_id", new mongoose.Types.ObjectId(id))

        if (!user) {
            throw new NotFoundError(`User not found`)
        }

        for(let [propName, propValue] of Object.entries(data)) {
            user[propName] = propValue
        }
        await user.save()
        return user
    }

}

module.exports = UserRepository