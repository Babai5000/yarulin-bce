const mongoose = require('mongoose')

const Difficulty = new mongoose.Schema({
    level: {
        type: String,
        required: true,
        unique: true
    }
},
{ _id : false })

const DifficultyModel = mongoose.model(`Difficulties`, Difficulty)

module.exports = DifficultyModel