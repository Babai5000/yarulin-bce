const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')

const Schema = mongoose.Schema

const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    rating: {
        type: Number,
        default: 0
    },
    difficulty: [String],
    lessons: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Lessons"
    }],
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comments"
    }],

})
courseSchema.plugin(timestamps, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
})

const CourseModel = mongoose.model(`Courses`, courseSchema)

module.exports = CourseModel