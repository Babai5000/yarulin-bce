const express = require('express')

const coursesRouter = require('./courses')
const categoriesRouter = require('./categories')
const difficultiesRouter = require("./difficulty")
const lessonRouter = require("./lessons")
const roleRouter = require("./roles")
const resourcesRouter = require("./resources")
const userRouter = require("./users")
const ratingRouter = require("./ratings")
const executionStatusRouter = require("./execution_statuses")
const certificatesRouter = require("./certificates")
const commetsRouter = require("./comments")
const resourcesTypeRouter = require("./resources_type")

const router = express.Router()

router.get('/', (req, res) => {
    res.send(`API V1`)
})

router.use("/courses", coursesRouter)
router.use("/categories", categoriesRouter)
router.use("/difficulties", difficultiesRouter)
router.use("/lessons", lessonRouter)
router.use("/roles", roleRouter)
router.use("/resources", resourcesRouter)
router.use("/users", userRouter)
router.use("/ratings", ratingRouter)
router.use("/execution-statuses", executionStatusRouter)
router.use("/certificates", certificatesRouter)
router.use("/comments", commetsRouter)
router.use("/resourse-types", resourcesTypeRouter)

module.exports = router