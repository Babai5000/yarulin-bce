const express = require('express')

const coursesRouter = express.Router()

coursesRouter.get("/", (req, res, next) => {
    res.send(`get all courses`)
})

coursesRouter.post("/", (req, res, next) => {
    res.send(`create course`)
})



coursesRouter.get("/:id", (req, res, next) => {
    res.send(`get course by id ${req.params.id}`)
})

coursesRouter.get("/:id/lessons", (req, res, next) => {
    res.send(`get course by id ${req.params.id} with lessons`)
})

coursesRouter.get("/:id/comments", (req, res, next) => {
    res.send(`get course by id ${req.params.id} with comments`)
})


coursesRouter.put("/:id", (req, res, next) => {
    res.send(`update course by id`)
})

coursesRouter.delete("/:id", (req, res, next) => {
    res.send(`delete course by id`)
})


module.exports = coursesRouter