const mongoose = require('mongoose')

const Schema = mongoose.Schema

const ResourceType = new Schema({
    type: {
        type: String,
        required: true,
        unique: true
    }
})

const ResourceTypesModel = mongoose.model(`ResourceTypes`, ResourceType)

module.exports = ResourceTypesModel