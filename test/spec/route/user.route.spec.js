const request = require('supertest')
const { app, server } = require('../../../server')
const MongooseConnect = require("../../../src/db/MongooseConnect")
const mongoose = require("mongoose")

const apiUrl = "/api/v1"

afterAll(async () => {
  await server.close()
  await MongooseConnect.disconnect()
})

describe('Test routes USERS', () => {

  let createdUserID = null

  describe('Post user POST /users', () => {

    describe('Create new user with success', () => {

      const userData = {
        name: "Test",
        surname: "Test",
        login: "Test",
        email: "test@test.ru",
        password: "1234567",
        info: "onfo data",
      }

      const association = { certificates: [], comments: [], courses: [], lessons: [] }

      test('Create new user with success', async () => {

        const res = await request(app)
          .post(`${apiUrl}/users`)
          .send(userData)
          .set('Accept', 'application/json')

        expect(res.headers["content-type"]).toMatch(/json/)
        expect(res.status).toEqual(201)

        const userDataFromServer = { ...res.body.user }
        delete userDataFromServer.createdAt
        delete userDataFromServer.updatedAt
        delete userDataFromServer.__v
        delete userDataFromServer._id

        expect(userDataFromServer).toEqual({ ...userData, ...association })
        createdUserID = res.body.user._id

      })

      test('Create new user with fail duplicate login', async () => {

        const res = await request(app)
          .post(`${apiUrl}/users`)
          .send(userData)
          .set('Accept', 'application/json')

        expect(res.headers["content-type"]).toMatch(/json/)
        expect(res.status).toEqual(500)
        expect(res.body.error).toMatch(/.*duplicate.*login.*/)

      })

    })

  })

  describe('Get all users GET /users', () => {
    test('Res is json data and has users', async () => {
      const res = await request(app)
        .get(`${apiUrl}/users`)
        .set('Accept', 'application/json')

      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(200)
      expect(res.body.users).toHaveLength(1)
    })
  })

  describe('Get user by id GET /users/:id', () => {
    test('Res is json data and has users', async () => {
      const res = await request(app)
        .get(`${apiUrl}/users/${createdUserID}`)
        .set('Accept', 'application/json')

      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(200)
      expect(res.body.user.login).toEqual(`Test`)

    })
  })

  describe('Put user PUT /users/:id', () => {

    const userData = {
      name: "Test123",
      surname: "Test123",
      login: "Test123",
      email: "test@test.ru",
      password: "1234567",
      info: "onfo data"
    }

    test('Update new user success', async () => {
      const res = await request(app)
        .put(`${apiUrl}/users/${createdUserID}`)
        .send(userData)
        .set('Accept', 'application/json')

      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(200)
      expect(res.body.user.name).toEqual(userData.name)
      expect(res.body.user.surname).toEqual(userData.surname)
      expect(res.body.user.login).toEqual(userData.login)
    })

    test('Update new user success fail incorrect _id', async () => {
      const res = await request(app)
        .put(`${apiUrl}/users/${createdUserID + 1111}`)
        .send(userData)
        .set('Accept', 'application/json')

      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(500)
      expect(res.body.error).toEqual(`input must be a 24 character hex string, 12 byte Uint8Array, or an integer`)
    })

    test('Update new user success fail _id', async () => {
      const failUserID = (`` + createdUserID).slice(0, -4) + 1111
      const res = await request(app)
        .put(`${apiUrl}/users/${failUserID}`)
        .send(userData)
        .set('Accept', 'application/json')

      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(404)
      expect(res.body.error).toMatch(/User not found/)

    })
  })

  describe('Delete user by id DELETE /users/:id', () => {
    test('Fail delete, incorrect ip', async () => {
      const failUserID = (`` + createdUserID).slice(0, -4) + 2222
      const res = await request(app)
        .delete(`${apiUrl}/users/${failUserID}`)
        .set('Accept', 'application/json')

      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(404)
      expect(res.body.error).toMatch(/User not found/)

    })

    test('Delete user success', async () => {
      const res = await request(app)
        .delete(`${apiUrl}/users/${createdUserID}`)
        .set('Accept', 'application/json')
      expect(res.headers["content-type"]).toMatch(/json/)
      expect(res.status).toEqual(200)

    })
  })

  //TODO auth

})