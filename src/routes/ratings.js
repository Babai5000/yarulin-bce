const express = require('express')

const ratingRouter = express.Router()

ratingRouter.get("/", (req, res, next) => {
    res.send(`get all ratings`)
})

ratingRouter.get("/courses/:id", (req, res, next) => {
    res.send(`get ratings by course id ${req.params.id}`)
})

ratingRouter.get("/:id", (req, res, next) => {
    res.send(`get rating by id ${req.params.id}`)
})

ratingRouter.post("/", (req, res, next) => {
    res.send(`create rating`)
})

ratingRouter.put("/:id", (req, res, next) => {
    res.send(`edit rating by id ${req.params.id}`)
})

ratingRouter.delete("/:id", (req, res, next) => {
    res.send(`delete rating by id ${req.params.id}`)
})


module.exports = ratingRouter