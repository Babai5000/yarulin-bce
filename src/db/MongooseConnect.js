const mongoose = require("mongoose")

class MongooseConnect {

    hasConnect = false

    constructor() {}

    connect (db_endpoint) {
        if (!this.hasConnect) {
            
            mongoose.connect(db_endpoint)
                .then(() => {
                    console.log(`connect to ${db_endpoint}`)
                    
                })
                .catch(e => console.error(e))
            
            this.hasConnect = true
        }
    }

    async disconnect () {
        if (this.hasConnect) {
            this.hasConnect = false
            await mongoose.connection.close()
        }
    }

}

module.exports = new MongooseConnect()