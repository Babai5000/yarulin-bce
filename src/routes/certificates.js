const express = require('express')

const certificatesRouter = express.Router()

certificatesRouter.get("/", (req, res, next) => {
    res.send(`get all certificates`)
})

certificatesRouter.get("/courses/:id", (req, res, next) => {
    res.send(`get certificate by course id ${req.params.id}`)
})

certificatesRouter.get("/:id", (req, res, next) => {
    res.send(`get certificates by id`)
})

certificatesRouter.post("/", (req, res, next) => {
    res.send(`create certificates`)
})

certificatesRouter.put("/:id", (req, res, next) => {
    res.send(`update certificates by id`)
})

certificatesRouter.delete("/:id", (req, res, next) => {
    res.send(`delete certificates by id`)
})

module.exports = certificatesRouter