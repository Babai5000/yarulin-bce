const express = require('express')

const difficultiesRouter = express.Router()

difficultiesRouter.get("/", (req, res, next) => {
    res.send(`get all difficulties`)
})

module.exports = difficultiesRouter