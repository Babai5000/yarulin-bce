const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')

const CommentTarget = new mongoose.Schema({
    typeTarget: {
        type: String,
        enum: ['Course', 'Lesson', 'Comment'],
        default: 'Course'
    },
    targetId: {
        type: mongoose.Schema.Types.ObjectId
    }
}, { _id : false })

const Comment = new mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users"
    },
    commentTarget: CommentTarget
})
Comment.plugin(timestamps,{ createdAt: 'createdAt', updatedAt: 'updatedAt' })

const CommentModel = mongoose.model(`Comments`, Comment)

module.exports = CommentModel