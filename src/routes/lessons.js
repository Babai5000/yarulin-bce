const express = require('express')

const lessonRouter = express.Router()

lessonRouter.get("/", (req, res, next) => {
    res.send(`get all lesson`)
})

lessonRouter.post("/", (req, res, next) => {
    res.send(`create lesson`)
})

lessonRouter.get("/:id", (req, res, next) => {
    res.send(`get lesson by id ${req.params.id}`)
})

lessonRouter.get("/:id/comments", (req, res, next) => {
    res.send(`get lesson by id ${req.params.id} with comments`)
})

lessonRouter.put("/:id", (req, res, next) => {
    res.send(`update lesson by id`)
})

lessonRouter.delete("/:id", (req, res, next) => {
    res.send(`delete lesson by id`)
})


module.exports = lessonRouter