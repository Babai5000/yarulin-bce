require('dotenv').config()

process.env.NODE_ENV = process.env.NODE_ENV || "development";

const config = {
    ...process.env
}

module.exports.config = config