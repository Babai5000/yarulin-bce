const express = require('express')

const commetsRouter = express.Router()

commetsRouter.get("/", (req, res, next) => {
    res.send(`get all comments`)
})

commetsRouter.get("/users/:id", (req, res, next) => {
    res.send(`get comments by user id ${req.params.id}`)
})

commetsRouter.get("/:id", (req, res, next) => {
    res.send(`get comments by id`)
})

commetsRouter.post("/", (req, res, next) => {
    res.send(`create comment`)
})

commetsRouter.put("/:id", (req, res, next) => {
    res.send(`update comments by id`)
})

commetsRouter.delete("/:id", (req, res, next) => {
    res.send(`delete comments by id`)
})

module.exports = commetsRouter