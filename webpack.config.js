const path = require('path');
const { config } = require('./src/configs/config')

module.exports = {
    mode: process.env.NODE_ENV,
    entry: {
        "common": './ui_src/common.js', 
        "auth": './ui_src/auth.js',
        "about": './ui_src/about.js',
        "courses": './ui_src/courses.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
    },
};