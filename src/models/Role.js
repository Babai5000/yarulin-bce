const mongoose = require('mongoose')

const Schema = mongoose.Schema

const Role = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
})

const RolesModel = mongoose.model(`Roles`, Role)

module.exports = RolesModel