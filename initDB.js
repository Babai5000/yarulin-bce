
const mongoose = require('mongoose')

const { config } = require('./src/configs/config')
const dbEndpoint = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`

const initCollections = async () => {

    const successFN = (c) =>  {
        console.log(`Collection ${c.name} created`)
    }
    const errorHandler = (e) => console.log(`Cant create collection. Error: ${e.message}`, e)

    //Коннект к базе данных
    const MongooseConnect = require("./src/db/MongooseConnect")
    await MongooseConnect.connect(dbEndpoint)

    //Удаление уже существующих коллекций
    const collections = await mongoose.connection.db.collections()
    console.log(collections)
    for (let collection of collections) {
        await collection.drop()
    }

    const CourseModel = require("./src/models/Course")
    const DifficultyModel = require("./src/models/Difficulty")
    const RolesModel = require("./src/models/Role")
    const ResourceTypesModel = require("./src/models/ResourceType")
    const ResourceModel = require("./src/models/Resource")
    const CategoryModel = require("./src/models/Category")
    const ExecutionStatusModel = require("./src/models/ExecutionStatus")
    const CommentModel = require("./src/models/Comment")
    const UserModel = require("./src/models/User")
    const RatingModel = require("./src/models/Rating")
    const CertificateModel = require("./src/models/Certificate")

    const models = [
        CourseModel,
        DifficultyModel,
        RolesModel,
        ResourceTypesModel,
        ResourceModel,
        CategoryModel,
        ExecutionStatusModel,
        CommentModel,
        UserModel,
        RatingModel,
        CertificateModel
    ]

    for (let model of models) {
        await model.createCollection().then(successFN).catch(errorHandler)
    }
    
    await MongooseConnect.disconnect()
}
initCollections()