const express = require('express')

const executionStatusRouter = express.Router()

executionStatusRouter.get("/", (req, res, next) => {
    res.send(`get all execution statuses`)
})

executionStatusRouter.get("/:target", (req, res, next) => {
    const targets = [`lesson`, `course`]
    if (!targets.includes(req.params.target)) {
        next()
        return
    }
    res.send(`get all execution statuses by target ${req.params.target}`)
})

module.exports = executionStatusRouter