const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')

const CourseUser = new mongoose.Schema({
    executionStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ExecutionStatuses"
    },
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Courses"
    }
})

const LessonUser = new mongoose.Schema({
    executionStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ExecutionStatuses"
    },
    lessonId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Lessons"
    }
})

const CertificateUser = new mongoose.Schema({
    path: {
        type: String,
        required: true,
    },
    certificateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Certificates"
    }
})
CertificateUser.plugin(timestamps,{ createdAt: 'createdAt' })

const User = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    surname: {
        type: String,
        required: true,
    },
    login: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    info: {
        type: String,
    },

    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Roles"
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comments"
    }],

    courses: [CourseUser],
    lessons: [LessonUser],
    certificates: [CertificateUser]

})
User.plugin(timestamps,{ createdAt: 'createdAt', updatedAt: 'updatedAt' })

const UserModel = mongoose.model(`Users`, User)

module.exports = UserModel