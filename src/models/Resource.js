const mongoose = require('mongoose')

const Schema = mongoose.Schema

const Resource = new Schema({
    data: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    }
})

const ResourceModel = mongoose.model(`Resources`, Resource)

module.exports = ResourceModel