const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')

const Rating = new mongoose.Schema({
    rating: {
        type: String,
        requered: true
    },
    description: {
        type: String,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users"
    },
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Courses"
    },
})
Rating.plugin(timestamps,{ createdAt: 'createdAt'})

const RatingModel = mongoose.model(`Rating`, Rating)

module.exports = RatingModel