const mongoose = require("mongoose")
const CourseModel = require("../models/Course")


var CourseRepository = class CourseRepository {

    async createCourse(courseData) {
        const course = new CourseModel({ ...courseData })
        await course.save()
    }

    async getOneByFeild(filedName, value) {
        return await CourseModel.findOne({[filedName]: value})
    }

    async getOneById(id) {
        return await this.getOneByFeild("_id", new mongoose.Types.ObjectId(id))
    }

    //TODO при удалении курса удалить все связанные уроки и пользовательские связи и сертификаты пользователей
    async deleteOneById(id) {
        return await CourseModel.deleteOne({ _id: new mongoose.Types.ObjectId(id)})
    }

}

module.exports = CourseRepository