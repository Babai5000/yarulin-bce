const express = require('express')
const mongoose = require('mongoose')
require('express-group-routes')
const { config } = require('./src/configs/config')
const router = require('./src/routes/index')
const path = require('path')

//Коннект к базе данных
const dbEndpoint = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`
const MongooseConnect = require("./src/db/MongooseConnect")
MongooseConnect.connect(dbEndpoint)



const app = express()
const port = config.SERVER_PORT

console.log(path.join(__dirname, 'dist'))
app.use('/static', express.static(path.join(__dirname, 'dist')))
app.set('views', './src/views')
app.set('view engine', 'pug')
app.use(express.urlencoded({extended: true}))
app.use(express.json())


app.get('/', (req, res) => {
    res.redirect(301, '/courses')
})
app.get('/courses', (req, res) => {
    res.render('courses', { title: 'Courses', page: `courses`})
})

app.get('/about', (req, res) => {
    res.render('about', { title: 'About', page: `about`})
})

app.get('/auth', (req, res) => {
    res.render('auth', { title: 'Auth', page: `auth`})
})

app.use("/api/v1", router);

const server = app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

app.on("error", (e) => {
    console.log(e.message)
    process.exit(1);
});

process.on("unhandledRejection", (e) => {
    console.log(e.message)
    process.exit(1)
})


module.exports = { app, server }
