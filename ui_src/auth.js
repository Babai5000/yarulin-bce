window.onload = (event) => {

    const signInForm = document.getElementById("signInForm")
    if (signInForm) {
        signInForm.addEventListener("submit", async (e) => {
            e.preventDefault()
    
            const formData = new FormData(signInForm)
            const jsonData = JSON.stringify(Object.fromEntries(formData))
    
            try {
                let response = await fetch('/api/v1/users/auth', {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: jsonData
                }).then(res => {
                    if (res.status !== 200) {
                        throw new Error(`Cant auth system`)
                    }
                    return res
                }).catch(e => {
                    throw e
                })
        
                await response.json()
                window.systemMessage.showSystemMessage({message: "Авторизация прошла успешно", type: `success`})
            } catch (e) {
                console.log(e.message)
                window.systemMessage.showSystemMessage({message: `Не удалось авторизоваться`, type: `error`})
            }
        })
    }

    
}