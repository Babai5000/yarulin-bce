const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')

const Schema = mongoose.Schema

const Lesson = new Schema({
    title: { 
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    lessonTime: {
        type: Number,
        default: 90
    },
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Courses"
    },
    resources: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Resources"
    }],
})
Lesson.plugin(timestamps,{
    createdAt: 'createdAt', 
    updatedAt: 'updatedAt'
})

const LessonModel = mongoose.model(`Lessons`, Lesson)

module.exports = LessonModel