const express = require('express')
const mongoose = require('mongoose')
require('express-group-routes')
const { config } = require('./src/configs/config')
const router = require('./src/routes/index')

//Коннект к базе данных
const MongooseConnect = require("./src/db/MongooseConnect")
const CourseRepository = require('./src/repository/CourseRepository')

const main = async () => {
    console.log(`main`)
    const courseRepository = new CourseRepository()
    // data = {
    //     title: `new course6`,
    //     description: `test course`,
    //     slug: "courses/new_course6",
    // }

    
    // await courseRepository.createCourse(data)

    const course = await courseRepository.deleteOneById("65a7be2b56bf2e9196f4a898")
    console.log(course)

    MongooseConnect.disconnect()
}
main()

