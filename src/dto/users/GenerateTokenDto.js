const GenerateTokenDto = class GenerateTokenDto {
    constructor(userData) {
        this.id = userData._id
        this.name = userData.name
        this.login = userData.login
    }

    toObject () {
        return {
            id: this.id,
            name: this.name,
            login: this.login
        }
    }
}