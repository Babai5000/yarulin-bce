const express = require('express')

const categoriesRouter = express.Router()

categoriesRouter.get("/", (req, res, next) => {
    res.send(`get all course categories`)
})

categoriesRouter.get("/:id", (req, res, next) => {
    res.send(`get course categories by id`)
})

categoriesRouter.post("/", (req, res, next) => {
    res.send(`create course categories`)
})

categoriesRouter.put("/:id", (req, res, next) => {
    res.send(`update course categories by id`)
})

categoriesRouter.delete("/:id", (req, res, next) => {
    res.send(`delete course categories by id`)
})

module.exports = categoriesRouter