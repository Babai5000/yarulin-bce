const messageType = {
    system: ["bg-white", "text-dark"],
    error: ["bg-danger", "text-white"],
    success: ["bg-success", "text-white"],
    warning: ["bg-warning", "text-dark"],
}

const setMessageProps = ({ title = `Системное сообщение`, message = `System message`, type = `system` }) => {
    const msgClasses = !messageType.hasOwnProperty(type) ? messageType.system : messageType[type]
    const modalTitle = document.getElementById('systemMessageToggleLabel')
    const modalText = document.getElementById('systemMessageText')
    const removeClasses = [...modalText.classList].filter(i => i !== "modal-body")
    modalText.classList.remove(...removeClasses)
    modalText.classList.add(...msgClasses)
    modalTitle.textContent = title
    modalText.textContent = message
}

const systemMessageModal = document.getElementById('systemMessage')

systemMessageModal.addEventListener('hidden.bs.modal', function (event) {
    setMessageProps({title: `Системное сообщение`, message: `System message`, type: `system`})
})

const systemMessage = bootstrap.Modal.getOrCreateInstance(systemMessageModal)
systemMessage.showSystemMessage = ({ title = `Системное сообщение`, message = `System message`, type = `system` }) => {
    setMessageProps({ title, message, type })
    systemMessage.show()
}

window.systemMessage = systemMessage