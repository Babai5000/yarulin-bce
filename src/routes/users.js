const express = require('express')
//const UserModel = require('../models/User')
const UserRepository = require('../repository/UserRepository')
const bcrypt = require('bcryptjs')
const { verify, sign } = require("jsonwebtoken")
const GenerateTokenDto = require("./../dto/users/GenerateTokenDto")

const userRouter = express.Router()
const userRepository = new UserRepository()

const LIFE_TIME_ACCESS_TOKEN = 60 * 60
const LIFE_TIME_REFRESH_TOKEN = 60 * 60 * 6


const generateTokens = (generateTokenDto) => {
    
    const jwtAlgorithm = process.env.JWT_ALGORITHM
    const jwtSecret = process.env.JWT_SECRET

    const payload = generateTokenDto.toObject()
    const jwtOptions = { algorithm: jwtAlgorithm, subject: user.login }

    const jwtAccessOptions = { ...jwtOptions, expiresIn: LIFE_TIME_ACCESS_TOKEN }
    const jwtRefreshOptions = { ...jwtOptions, expiresIn: LIFE_TIME_REFRESH_TOKEN }

    return {
        accessToken: sign(payload, jwtSecret, jwtAccessOptions),
        refreshToken: sign(payload, jwtSecret, jwtRefreshOptions),
    }

}

userRouter.post("/auth", async (req, res, next) => {

    const { login, password } = req.body
    const user = (await userRepository.getOneByFeild(`login`, login)).toObject()

    if (!user) {
        res.status(400).json({status: 400, message: `Incorrect password or login`})
        return
    }

    const verifyPassword = bcrypt.compareSync(password, user.password)
    if(!verifyPassword) {
        res.status(401).json({status: 401, message: `Incorrect password or login`})
        return
    }

    const generateTokenDto = new GenerateTokenDto({ ...user })
    const tokens = generateTokens(generateTokenDto)

    res.status(200).json({status: 200, tokens})
})

userRouter.get("/", async (req, res, next) => {

    try {
        const users = await userRepository.getAll()
        res.json({ users: users })
    } catch (e) {
        res.status(500).json({ error: e.message })
    }

})

userRouter.post("/", async (req, res, next) => {

    try {
        const hashedPass = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8))
        let user = (await userRepository.create({ ...req.body, password: hashedPass })).toObject()
        //console.log(user.toObject())
        delete user.password
        delete user.__v
        
        res.status(201).json({ user })
    } catch (e) {
        res.status(500).json({ error: e.message })
    }

})

userRouter.get("/:id", async (req, res, next) => {

    try {
        const user = await userRepository.getOneById(req.params.id)
        res.json({ user })
    } catch (e) {
        res.status(500).json({ error: e.message })
    }

})

userRouter.get("/:id/courses", (req, res, next) => {
    res.send(`get user by id ${req.params.id} with courses`)
})

userRouter.put("/:id", async (req, res, next) => {
    try {
        const user = await userRepository.update(req.params.id, req.body)
        res.json({ user })
    } catch (e) {
        console.log(e.message)
        const statusCode = e.isApiExection ? e.statusCode : 500
        res.status(statusCode).json({ error: e.message })
    }

})

userRouter.delete("/:id", async (req, res, next) => {

    try {
        await userRepository.deleteOneById(req.params.id)
        res.json({ message: `Ok` })
    } catch (e) {
        const statusCode = e.isApiExection ? e.statusCode : 500
        res.status(statusCode).json({ error: e.message })
    }
    
})


module.exports = userRouter