const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')

const Certificate = new mongoose.Schema({
    templatePath: {
        type: String,
        required: true
    },
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Courses"
    },
})
Certificate.plugin(timestamps,{ createdAt: 'createdAt', updatedAt: 'updatedAt' })

const CertificateModel = mongoose.model(`Certificates`, Certificate)

module.exports = CertificateModel