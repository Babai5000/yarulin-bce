const mongoose = require('mongoose')

const Category = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
    }
},
{ _id : false })

const CategoryModel = mongoose.model(`Categories`, Category)

module.exports = CategoryModel