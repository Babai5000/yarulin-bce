const express = require('express')

const resourcesRouter = express.Router()

resourcesRouter.get("/", (req, res, next) => {
    res.send(`get all resources`)
})

resourcesRouter.get("/:id", (req, res, next) => {
    res.send(`get all resources`)
})

resourcesRouter.post("/", (req, res, next) => {
    res.send(`create resource`)
})

resourcesRouter.put("/:id", (req, res, next) => {
    res.send(`edit resource`)
})

resourcesRouter.delete("/:id", (req, res, next) => {
    res.send(`delete resource id`)
})


module.exports = resourcesRouter