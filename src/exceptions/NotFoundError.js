var NotFoundError = class NotFoundError extends Error {
    statusCode = 404
    isApiExection = true

    constructor(message = `Not found`) {
        super(message)
    }
}

module.exports = NotFoundError