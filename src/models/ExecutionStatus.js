const mongoose = require('mongoose')

const ExecutionStatus = new mongoose.Schema({
    status: {
        type: String,
        required: true,
        unique: true
    },
    executionEntity: {
        type: String,
        enum: ['Course', 'Lesson'],
        default: 'Course'
    }
}).index({ status: 1, executionEntity: 1 }, { unique: true })

const ExecutionStatusModel = mongoose.model(`ExecutionStatuses`, ExecutionStatus)

module.exports = ExecutionStatusModel